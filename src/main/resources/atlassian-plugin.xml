<atlassian-plugin name="${project.name}" key="${atlassian.plugin.key}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <vendor name="Atlassian Pty Ltd" url="http://www.atlassian.com/"/>
        <version>${project.version}</version>
        <param name="configure.url">/admin/plugins/impresence2/config.action</param>
        <bundle-instructions>
            <Export-Package>
                com.atlassian.confluence.extra.impresence2.reporter
            </Export-Package>
        </bundle-instructions>
    </plugin-info>

    <resource type="i18n" name="i18n" location="com/confluence/extra/impresence2/i18n"/>

    <component key="localeSupport" class="com.atlassian.confluence.extra.impresence2.util.DefaultLocaleSupport">
        <description>Provides i18n support for presence reporters.</description>
        <interface>com.atlassian.confluence.extra.impresence2.util.LocaleSupport</interface>
    </component>

    <component key="presenceManager" class="com.atlassian.confluence.extra.impresence2.DefaultPresenceManager">
        <description>Manages presence reporters</description>
        <interface>com.atlassian.confluence.extra.impresence2.PresenceManager</interface>
    </component>

    <module-type key="presence-reporter" class="com.atlassian.confluence.extra.impresence2.PresenceReporterModuleDescriptor"/>

    <presence-reporter key="reporter-skypeme" class="com.atlassian.confluence.extra.impresence2.reporter.SkypePresenceReporter">
        <description>Reports Skype presence</description>
    </presence-reporter>
    <presence-reporter key="reporter-skype" class="com.atlassian.confluence.extra.impresence2.reporter.SkypePresenceReporter">
        <description>Reports Skype presence</description>
    </presence-reporter>

    <presence-reporter key="reporter-jabber" class="com.atlassian.confluence.extra.impresence2.reporter.JabberPresenceReporter">
        <description>Reports generic Jabber presence</description>
    </presence-reporter>

    <presence-reporter key="reporter-aim" class="com.atlassian.confluence.extra.impresence2.reporter.AIMPresenceReporter">
        <description>Reports AIM presence</description>
    </presence-reporter>
    <presence-reporter key="reporter-yahoo" class="com.atlassian.confluence.extra.impresence2.reporter.YahooPresenceReporter">
        <description>Reports Yahoo presence</description>
    </presence-reporter>
    <presence-reporter key="reporter-sametime" class="com.atlassian.confluence.extra.impresence2.reporter.SametimePresenceReporter">
        <description>Reports Sametime (experimental) presence</description>
    </presence-reporter>
    <presence-reporter key="reporter-wildfire" class="com.atlassian.confluence.extra.impresence2.reporter.WildfirePresenceReporter">
        <description>Reports Wildfire presence</description>
    </presence-reporter>
    <presence-reporter key="reporter-icq" class="com.atlassian.confluence.extra.impresence2.reporter.ICQPresenceReporter">
        <description>Reports ICQ presence</description>
    </presence-reporter>

    <macro name="im" class="com.atlassian.confluence.extra.impresence2.IMMacro" key="im" hidden="true">
        <description>Displays a status graphic for a specified IM id (Legacy).</description>
        <category name="external-content"/>
    </macro>
    <xhtml-macro name="im" class="com.atlassian.confluence.extra.impresence2.IMMacro" key="im-xhtml" hidden="true">
        <description>Displays a status graphic for a specified IM id.</description>
        <category name="external-content"/>
    </xhtml-macro>

    <macro name="aim" class="com.atlassian.confluence.extra.impresence2.OldAimMacro" key="aim" hidden="true">
        <description>Old style AIM presence macro (Legacy)</description>
        <category name="external-content"/>
    </macro>
    <xhtml-macro name="aim" class="com.atlassian.confluence.extra.impresence2.OldAimMacro" key="aim-xhtml" hidden="true">
        <description>Old style AIM presence macro</description>
        <category name="external-content"/>
    </xhtml-macro>

    <macro name="icq" class="com.atlassian.confluence.extra.impresence2.OldIcqMacro" key="icq" hidden="true">
        <description>Old style ICQ presence macro (Legacy)</description>
        <category name="external-content"/>
    </macro>
    <xhtml-macro name="icq" class="com.atlassian.confluence.extra.impresence2.OldIcqMacro" key="icq-xhtml" hidden="true">
        <description>Old style ICQ presence macro</description>
        <category name="external-content"/>
    </xhtml-macro>

    <macro name="sametime" class="com.atlassian.confluence.extra.impresence2.OldSametimeMacro" key="sametime" hidden="true">
        <description>Old style Sametime presence macro (Legacy)</description>
        <category name="external-content"/>
    </macro>
    <xhtml-macro name="sametime" class="com.atlassian.confluence.extra.impresence2.OldSametimeMacro" key="sametime-xhtml" hidden="true">
        <description>Old style Sametime presence macro</description>
        <category name="external-content"/>
    </xhtml-macro>

    <web-resource name="sametime web resources" key="sametime-resources">
        <description>Scripts used by the sametime macro</description>
        <resource name="sametime.js" type="download" location="templates/extra/impresence2/sametime.js"/>
        <dependency>confluence.web.resources:ajs</dependency>
    </web-resource>

    <macro name="skype" class="com.atlassian.confluence.extra.impresence2.OldSkypeMacro" key="skype" hidden="true">
        <description>Old style Skype presence macro (Legacy)</description>
        <category name="external-content"/>
    </macro>
    <xhtml-macro name="skype" class="com.atlassian.confluence.extra.impresence2.OldSkypeMacro" key="skype-xhtml" hidden="true">
        <description>Old style Skype presence macro</description>
        <category name="external-content"/>
    </xhtml-macro>

    <macro name="skypeme" class="com.atlassian.confluence.extra.impresence2.OldSkypemeMacro" key="skypeme" hidden="true">
        <description>Old style Skypeme presence macro (Legacy)</description>
        <category name="external-content"/>
    </macro>
    <xhtml-macro name="skypeme" class="com.atlassian.confluence.extra.impresence2.OldSkypemeMacro" key="skypeme-xhtml" hidden="true">
        <description>Old style Skypeme presence macro</description>
        <category name="external-content"/>
    </xhtml-macro>

    <macro name="wildfire" class="com.atlassian.confluence.extra.impresence2.OldWildfireMacro" key="wildfire" hidden="true">
        <description>Old style Wildfire presence macro (Legacy)</description>
        <category name="external-content"/>
    </macro>
    <xhtml-macro name="wildfire" class="com.atlassian.confluence.extra.impresence2.OldWildfireMacro" key="wildfire-xhtml" hidden="true">
        <description>Old style Wildfire presence macro</description>
        <category name="external-content"/>
    </xhtml-macro>

    <macro name="yahoo" class="com.atlassian.confluence.extra.impresence2.OldYahooMacro" key="yahoo" hidden="true">
        <description>Old style Yahoo presence macro (Legacy)</description>
        <category name="external-content"/>
    </macro>
    <xhtml-macro name="yahoo" class="com.atlassian.confluence.extra.impresence2.OldYahooMacro" key="yahoo-xhtml" hidden="true">
        <description>Old style Yahoo presence macro</description>
        <category name="external-content"/>
    </xhtml-macro>

    <macro-migrator key="skypeme-migrator" macro-name="skypeme" class="com.atlassian.confluence.extra.impresence2.migrator.OldImMacroMigrator">
        <description>Migrates the body of the Skypme presence macro</description>
    </macro-migrator>
    <macro-migrator key="wildfire-migrator" macro-name="wildfire" class="com.atlassian.confluence.extra.impresence2.migrator.OldImMacroMigrator">
        <description>Migrates the body of the Wildfire presence macro</description>
    </macro-migrator>

    <resource name="images/" type="download" location="templates/extra/impresence2/images/"/>

    <xwork name="IM Presence Configuration" key="impresence.config">
        <package name="IM" extends="default" namespace="/admin/plugins/impresence2">
            <action name="config" class="com.atlassian.confluence.extra.impresence2.config.IMConfigAction">
                <interceptor-ref name="defaultStack"/>
                <result name="input" type="velocity">/templates/extra/impresence2/im-config.vm</result>
                <result name="error" type="velocity">/templates/extra/impresence2/im-config.vm</result>
                <result name="success" type="velocity">/templates/extra/impresence2/im-config.vm</result>
            </action>
        </package>
        <package name="Jabber" extends="default" namespace="/admin/plugins/impresence2/jabber">
            <action name="config" class="com.atlassian.confluence.extra.impresence2.config.JabberPresenceConfigAction" method="doDefault">
                <interceptor-ref name="defaultStack"/>
                <result name="input" type="velocity">/templates/extra/impresence2/jabber-config.vm</result>
                <result name="error" type="velocity">/templates/extra/impresence2/jabber-config.vm</result>
                <result name="success" type="velocity">/templates/extra/impresence2/jabber-config.vm</result>
            </action>
            <action name="doconfig" class="com.atlassian.confluence.extra.impresence2.config.JabberPresenceConfigAction">
                <interceptor-ref name="validatingStack"/>
                <result name="input" type="velocity">/templates/extra/impresence2/jabber-config.vm</result>
                <result name="error" type="velocity">/templates/extra/impresence2/jabber-config.vm</result>
                <result name="success" type="velocity">/templates/extra/impresence2/jabber-config.vm</result>
            </action>
        </package>
        <package name="Sametime" extends="default" namespace="/admin/plugins/impresence2/sametime">
            <action name="config" class="com.atlassian.confluence.extra.impresence2.config.SametimePresenceConfigAction" method="doDefault">
                <interceptor-ref name="defaultStack"/>
                <result name="input" type="velocity">/templates/extra/impresence2/sametime-presence-config.vm</result>
                <result name="error" type="velocity">/templates/extra/impresence2/sametime-presence-config.vm</result>
                <result name="success" type="velocity">/templates/extra/impresence2/sametime-presence-config.vm</result>
            </action>
            <action name="doconfig" class="com.atlassian.confluence.extra.impresence2.config.SametimePresenceConfigAction">
                <interceptor-ref name="validatingStack"/>
                <result name="input" type="velocity">/templates/extra/impresence2/sametime-presence-config.vm</result>
                <result name="error" type="velocity">/templates/extra/impresence2/sametime-presence-config.vm</result>
                <result name="success" type="velocity">/templates/extra/impresence2/sametime-presence-config.vm</result>
            </action>
        </package>
        <package name="Wildfire" extends="default" namespace="/admin/plugins/impresence2/wildfire">
            <action name="config" class="com.atlassian.confluence.extra.impresence2.config.WildfirePresenceConfigAction" method="doDefault">
                <interceptor-ref name="defaultStack"/>
                <result name="input" type="velocity">/templates/extra/impresence2/wildfire-presence-config.vm</result>
                <result name="error" type="velocity">/templates/extra/impresence2/wildfire-presence-config.vm</result>
                <result name="success" type="velocity">/templates/extra/impresence2/wildfire-presence-config.vm</result>
            </action>
            <action name="doconfig" class="com.atlassian.confluence.extra.impresence2.config.WildfirePresenceConfigAction">
                <interceptor-ref name="validatingStack"/>
                <result name="input" type="velocity">/templates/extra/impresence2/wildfire-presence-config.vm</result>
                <result name="error" type="velocity">/templates/extra/impresence2/wildfire-presence-config.vm</result>
                <result name="success" type="velocity">/templates/extra/impresence2/wildfire-presence-config.vm</result>
            </action>
        </package>
    </xwork>
</atlassian-plugin>
