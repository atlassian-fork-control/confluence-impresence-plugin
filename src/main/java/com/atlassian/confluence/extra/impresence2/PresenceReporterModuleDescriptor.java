package com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.extra.impresence2.reporter.PresenceReporter;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

public class PresenceReporterModuleDescriptor extends AbstractModuleDescriptor<PresenceReporter>
{
    private PresenceReporter presenceReporter;

    public PresenceReporterModuleDescriptor(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    public PresenceReporter getModule()
    {
        synchronized (this)
        {
            if (null == presenceReporter)
                presenceReporter = moduleFactory.createModule(moduleClassName, this);
        }

        return presenceReporter;
    }
}
