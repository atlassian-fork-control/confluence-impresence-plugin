package com.atlassian.confluence.extra.impresence2.util;

import java.util.List;

public interface LocaleSupport
{
    String getText(final String key);

    String getText(final String key, final Object[] substitutions);

    String getText(final String key, final List list) ;

    String getTextStrict(final String key) ;
}
