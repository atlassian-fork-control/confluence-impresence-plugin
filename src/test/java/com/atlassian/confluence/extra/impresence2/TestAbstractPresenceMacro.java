package com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.extra.impresence2.reporter.PresenceException;
import com.atlassian.confluence.extra.impresence2.reporter.PresenceReporter;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Mockito.*;

public class TestAbstractPresenceMacro extends TestCase
{
    private static final String SERVICE_TEST = "test";

    private TestPresenceMacro presenceMacro;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private I18NBeanFactory i18NBeanFactory;

    @Mock
    private I18NBean i18NBean;

    @Mock
    private PresenceManager presenceManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private PresenceReporter presenceReporter;

    @Mock
    private VelocityHelperService velocityHelperService;

    private Map<String, String> macroParameters;

    private RenderContext renderContext;

    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        when(localeManager.getLocale(Matchers.<User>anyObject())).thenReturn(Locale.getDefault());
        when(i18NBeanFactory.getI18NBean(Matchers.<Locale>anyObject())).thenReturn(i18NBean);

        when(i18NBean.getText(anyString(), Matchers.<Object[]>anyObject())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );

        presenceMacro = new TestPresenceMacro(localeManager, i18NBeanFactory, presenceManager, permissionManager, velocityHelperService);

        macroParameters = new HashMap<String, String>();
        renderContext = new Page().toPageContext();
    }

    public void testExecuteWhenServiceNotSupported()
    {
        try
        {
            presenceMacro.execute(macroParameters, StringUtils.EMPTY, renderContext);
            fail("Control should not get to this point");
        }
        catch (final MacroException me)
        {
            assertEquals("com.atlassian.confluence.macro.MacroExecutionException: error.macro.unsupportedservice", me.getMessage());
        }
    }

    public void testExecuteWhenInvalidServiceIsSpecifiedByTheDefaultMacroParameter()
    {
        try
        {
            macroParameters.put("1", "fooservice");

            presenceMacro.execute(macroParameters, StringUtils.EMPTY, renderContext);
            fail("Control should not get to this point");
        }
        catch (final MacroException me)
        {
            assertEquals("com.atlassian.confluence.macro.MacroExecutionException: error.macro.unsupportedservice", me.getMessage());
        }
    }

    public void testExecuteWhenInvalidServiceIsSpecifiedByTheServiceParameter()
    {
        try
        {
            macroParameters.put("service", "fooservice");

            presenceMacro.execute(macroParameters, StringUtils.EMPTY, renderContext);
            fail("Control should not get to this point");
        }
        catch (final MacroException me)
        {
            assertEquals("com.atlassian.confluence.macro.MacroExecutionException: error.macro.unsupportedservice", me.getMessage());
        }
    }

    public void testExecuteWhenServiceSpecifiedRequiresConfiguration() throws MacroException, IOException, PresenceException
    {
        macroParameters.put("service", "fooservice");

        when(presenceManager.getReporter(SERVICE_TEST)).thenReturn(presenceReporter);
        when(presenceReporter.requiresConfig()).thenReturn(true);

        assertEquals("html", new TestPresenceMacro(localeManager, i18NBeanFactory, presenceManager, permissionManager, velocityHelperService)
        {
            @Override
            protected String getRenderedHtml(PresenceReporter reporter)
            {
                return "html";
            }
        }.execute(macroParameters, StringUtils.EMPTY, renderContext));

        verify(presenceReporter, never()).getPresenceXHTML(anyString(), anyBoolean());
    }

    public void testExecuteWhenServiceSpecifiedReturnsPresenceXhtml() throws MacroException, PresenceException, IOException
    {
        macroParameters.put("0", "fooid");
        macroParameters.put("service", "fooservice");

        when(presenceManager.getReporter(SERVICE_TEST)).thenReturn(presenceReporter);
        when(presenceReporter.getPresenceXHTML("fooid", true)).thenReturn("fooxhtml");

        assertEquals("fooxhtml", presenceMacro.execute(macroParameters, StringUtils.EMPTY, renderContext));
    }

    private static class TestPresenceMacro extends AbstractPresenceMacro
    {
        private TestPresenceMacro(LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, PresenceManager presenceManager, PermissionManager permissionManager, VelocityHelperService velocityHelperService)
        {
            super(localeManager, i18NBeanFactory, presenceManager, permissionManager, velocityHelperService);
        }

        @Override
        protected String getImService(Map<String, String> parameters)
        {
            return SERVICE_TEST;
        }
    }
}
