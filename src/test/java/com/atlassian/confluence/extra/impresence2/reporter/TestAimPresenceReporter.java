package com.atlassian.confluence.extra.impresence2.reporter;

import java.io.IOException;

public class TestAimPresenceReporter extends AbstractPresenceReporterTest<AIMPresenceReporter>
{
    protected String getPresenceReporterKey()
    {
        return AIMPresenceReporter.KEY;
    }

    protected AIMPresenceReporter createPresenceReporter()
    {
        return new AIMPresenceReporter(localeSupport);
    }

    public void testGetKey()
    {
        assertEquals("aim", AIMPresenceReporter.KEY);
    }

    public void testGetName()
    {
        assertEquals("presencereporter.aim.name", presenceReporter.getName());
    }

    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.aim.servicehomepage", presenceReporter.getServiceHomepage());
    }

    public void testHasConfig()
    {
        assertEquals(false, presenceReporter.hasConfig());
    }

    public void testRequiresConfig()
    {
        assertEquals(false, presenceReporter.requiresConfig());
    }

    public void testGetPresenceXHTMLWhenIdIsNotSpecified() throws IOException, PresenceException
    {
        assertTrue(presenceReporter.getPresenceXHTML(null, false).indexOf("presencereporter.aim.error.noscreenname") >= 0);
    }

    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";

        assertEquals(
                "<a href='aim:GoIM?screenname=" + id + "'><img src='http://api.oscar.aol.com/SOA/key=jo1rkjdnQ-LEjx49/presence/" + id + "' height='16' width='16' style='vertical-align:bottom; margin:0px 1px;' border='0'/></a>"
                        + "&nbsp;<a title=\"AIM " + id + "\" href=\"aim:GoIM?screenname=" + id + "\">" + id + "</a>",
                presenceReporter.getPresenceXHTML(id, true));
    }

    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";

        assertEquals("<a href='aim:GoIM?screenname=" + id + "'><img src='http://api.oscar.aol.com/SOA/key=jo1rkjdnQ-LEjx49/presence/" + id + "' height='16' width='16' style='vertical-align:bottom; margin:0px 1px;' border='0'/></a>",
                presenceReporter.getPresenceXHTML(id, false));
    }
}