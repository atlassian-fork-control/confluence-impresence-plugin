package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.extra.impresence2.reporter.ServerPresenceReporter;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;
public abstract class ServerPresenceConfigActionTestCase<A extends ServerPresenceConfigAction, T extends ServerPresenceReporter> extends AbstractIMConfigActionTestCase<A, T>
{
    protected T createReporter()
    {
        presenceReporter = mock(getPresenceReporterClass());
        when(presenceReporter.getKey()).thenReturn(getServiceKey());

        return presenceReporter;
    }

    public void testExecuteWhenServerNotSpecified() throws Exception
    {
        action.setServer(null);

        when(presenceReporter.getServer()).thenReturn("fakeServer");

        assertEquals("success", action.execute());

        verify(presenceReporter).setServer(null);
    }

    public void testExecuteWhenServerIsSpecified() throws Exception
    {
        action.setServer("newFakeServer");

        assertEquals("success", action.execute());

        verify(presenceReporter).setServer("newFakeServer");
    }

    protected abstract Class<T> getPresenceReporterClass();
}
