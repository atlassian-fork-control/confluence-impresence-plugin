package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.extra.impresence2.reporter.JabberPresenceReporter;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.xml.sax.SAXException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;

public class JabberPresenceTestCase extends AbstractPresenceTestCase
{
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getBandanaHelper("extra.im.account.jabber").delete();
        getBandanaHelper("extra.im.password.jabber").delete();
        getBandanaHelper("extra.im.domain.jabber").delete();
        getBandanaHelper("extra.im.port.jabber").delete();
    }

    protected boolean requiresConfiguration()
    {
        return true;
    }

    public void testRequiresConfig()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Requires Configuration");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=jabber}"
        );

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's Jabber (XMPP) service before you can use this macro.");
        assertLinkPresentWithText("Configure Jabber (XMPP) service");
    }

    public void testConfigurationNotAllowedForNonAdminUsers()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Dummy Account Configuration Accessible By Admins Only");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=jabber}"
        );

        assertTrue(pageHelper.create());

        logout();
        login(nonAdminUserName, nonAdminPassword);
        try
        {
            gotoPage("/");
            gotoPage("/display/" + testSpaceKey);

            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTextPresent("An administrator must configure your server's Jabber (XMPP) service before you can use this macro.");
            assertLinkNotPresentWithText("Configure Jabber (XMPP) service");
        }
        finally
        {
            logout();
            loginAsAdmin(); /* So tearDown can be called successfully */
        }
    }

    private void configureDummyAccount(final String reporterId, final String reporterPassword)
    {
        setWorkingForm("presenceconfigform");
        setTextField("reporterId", reporterId);
        setTextField("reporterPassword", reporterPassword);
        submit("update");

        /* Assert if the values are correctly set */
        assertTitleEquals("Configure Jabber (XMPP) service - Confluence");
        assertThat(getElementAttributeByXPath("//input[@name='reporterId']", "value"), is(reporterId));
        assertThat("never send passwords to the browser", getElementAttributeByXPath("//input[@name='reporterPassword']", "value"), is("********"));
        assertThat(getElementAttributeByXPath("//input[@name='domain']", "value"), is("chat.example.com"));
        assertThat(getElementAttributeByXPath("//input[@name='port']", "value"), is(Integer.toString(JabberPresenceReporter.DEFAULT_JABBER_PORT)));
    }

    public void testShowPresenceWithId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence With ID");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=jabber}"
        );

        assertTrue(pageHelper.create());

        try
        {
            // Configure Jabber Service requires escalated privileges
            gotoPageWithEscalatedPrivileges("/pages/viewpage.action?pageId=" + pageHelper.getId());

            clickLinkWithText("Configure Jabber (XMPP) service");

            assertTitleEquals("Configure Jabber (XMPP) service - Confluence");
            assertThat(getElementAttributeByXPath("//input[@name='reporterId']", "value"), isEmptyString());
            assertThat(getElementAttributeByXPath("//input[@name='reporterPassword']", "value"), isEmptyString());
            assertThat(getElementAttributeByXPath("//input[@name='domain']", "value"), is("chat.example.com"));
            assertThat(getElementAttributeByXPath("//input[@name='port']", "value"), is(Integer.toString(JabberPresenceReporter.DEFAULT_JABBER_PORT)));

            configureDummyAccount("admin@localhost.localdomain", "admin");

        	/* Now, let's see if the page shows the presence of the targeted user */
            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTextNotPresent("An administrator must configure your server's Jabber (XMPP) service before you can use this macro.");
        }
        finally
        {
            dropEscalatedPrivileges();
            assertTrue(getBandanaHelper("extra.im.account.jabber").delete());
            assertTrue(getBandanaHelper("extra.im.password.jabber").delete());
            assertTrue(getBandanaHelper("extra.im.domain.jabber").delete());
            assertTrue(getBandanaHelper("extra.im.port.jabber").delete());
        }
    }
}
